from pathlib import Path

import numpy as np
from PIL import Image
from torch.utils.data import DataLoader, Dataset

from .augmentations import get_augmentation
from .image_util import get_binary_image, normalize_image, preprocess_image_pretrained


def get_training_loader(
    data_dir,
    mode,
    diffusion_type,
    use_pretrained_model,
    batch_size,
    shuffle=False,
    resolution=None,
    augmentation=False,
    num_workers=0,
):
    """
    Returns batches of image-mask tensors and corresponding image name
    mode: one of ["training", "validation"]
    """
    if diffusion_type == "gaussian":
        mask_norm_fn = normalize_image
    elif diffusion_type == "bernoulli":
        mask_norm_fn = get_binary_image

    if use_pretrained_model:
        image_norm_fn = preprocess_image_pretrained
    else:
        image_norm_fn = normalize_image

    dataset = TrainDataset(
        Path(data_dir),
        mode,
        resolution,
        image_norm_fn=image_norm_fn,
        mask_norm_fn=mask_norm_fn,
        augmentation=augmentation,
    )
    return DataLoader(
        dataset,
        batch_size,
        shuffle=shuffle,
        num_workers=num_workers,
        pin_memory=True,
        drop_last=False,
    )


def get_test_dataloader(
    image_dir, use_pretrained_model, batch_size, shuffle=False, resolution=None
):
    """Returns images (without mask) and corresponding image name"""

    if use_pretrained_model:
        image_norm_fn = preprocess_image_pretrained
    else:
        image_norm_fn = normalize_image

    dataset = TestDataset(Path(image_dir), resolution, image_norm_fn)
    return DataLoader(
        dataset,
        batch_size,
        shuffle=shuffle,
        num_workers=0,
        pin_memory=True,
        drop_last=False,
    )


class TrainDataset(Dataset):
    """Dataset of image-mask tensors"""

    def __init__(
        self,
        data_dir,
        mode,  # one of ("training", "validation")
        resolution=None,
        image_norm_fn=None,
        mask_norm_fn=None,
        augmentation=False,
    ):
        super().__init__()
        self.image_paths = list(data_dir.glob(f"**/{mode}/images/*.png"))
        self.resolution = resolution
        self.image_norm_fn = image_norm_fn
        self.mask_norm_fn = mask_norm_fn
        if augmentation:
            self.transform = get_augmentation(resolution)
        else:
            self.transform = None

    def __len__(self):
        return len(self.image_paths)

    def __getitem__(self, idx):
        image_path = self.image_paths[idx]
        mask_path = image_path.parent.parent / "groundtruth" / image_path.name
        with Image.open(image_path) as image, Image.open(mask_path) as mask:
            if self.resolution is not None:
                image = image.resize((self.resolution, self.resolution))
                mask = mask.resize((self.resolution, self.resolution))

            np_image = np.array(image.convert("RGB"))
            np_mask = np.array(mask.convert("L"))

        if self.transform:
            transformed = self.transform(image=np_image, mask=np_mask)
            np_image, np_mask = transformed["image"], transformed["mask"]

        np_image = np_image.astype(np.float32).transpose([2, 0, 1])
        np_mask = np_mask.astype(np.float32)

        if self.image_norm_fn is not None:
            np_image = self.image_norm_fn(np_image)

        if self.mask_norm_fn is not None:
            np_mask = self.mask_norm_fn(np_mask)

        return np_image, np_mask


class TestDataset(Dataset):
    """Dataset with image tensors (without masks)"""

    def __init__(
        self,
        test_dir,
        resolution=None,
        image_norm_fn=None,
    ):
        super().__init__()
        self.image_paths = list(test_dir.glob("*.png"))
        self.resolution = resolution
        self.image_norm_fn = image_norm_fn

    def __len__(self):
        return len(self.image_paths)

    def __getitem__(self, idx):
        image_path = self.image_paths[idx]

        with Image.open(image_path) as image:
            if self.resolution is not None:
                image = image.resize((self.resolution, self.resolution))

            np_image = (
                np.array(image.convert("RGB")).astype(np.float32).transpose([2, 0, 1])
            )

        if self.image_norm_fn is not None:
            np_image = self.image_norm_fn(np_image)

        return np_image, image_path.name
